<?php

/**
 * This class validate passwords to have a minimum number of specific characters.
 * @author Lenscak José Francisco <malguzt@gmail.com>
 *
 */
class sfValidatorPassword extends sfValidatorString
{
    /**
     * Configures the current validator.
     * All parameters have default values, but can accept new values.
     * 
     * @param array $options  Optionals keys: min_length, max_length, min_lowercase, min_uppercas, min_special_character, min_number
     * @param array $messages Optionals keys: min_length, max_length, not_lowercase, not_uppercas, not_special_character, not_number
     */
    protected function configure($options = array(), $messages = array())
    {

        parent::configure($options, $messages);

        // This are default values. Can overwrite them in the declaration.
        $this->setOption('min_length', 8);
        $this->setOption('max_length', 20);
        $this->setMessage('min_length', 'The password must have at least %min_length% characters.');
        $this->setMessage('max_length', 'The password can have at most %max_length% characters.');

        $this->setMessage('required', 'The password cannot be empty.');

        $this->addMessage('not_lowercase', 'The password should have at least %min_lowercase% lowecase letters.');
        $this->addMessage('not_uppercase', 'The password should have at least %min_uppercase% uppercase letters.');
        $this->addMessage(
                'not_special_character',
                'The password should have at least %min_special_character% special characters. ($?+-!%)');
        $this->addMessage('not_number', 'The password should have at least %min_number% numbers.');

        $this->addOption('min_lowercase', 1);
        $this->addOption('min_uppercase', 1);
        $this->addOption('min_special_character', 1);
        $this->addOption('min_number', 1);
    }

    /**
     * @see sfValidatorBase
     * 
     * @param string $value
     * 
     * @throws sfValidatorError
     */
    protected function doClean($value)
    {
        $clean = parent::doClean($value);

        if ($this->haveLowercase($clean) && $this->haveUppercase($clean) && $this->haveSpecialCharacter($clean)
            && $this->haveNumber($clean)) {

            return $clean;
        }

        throw new sfValidatorError($this, 'invalid');
    }

    /**
     * Check if $value have min_lowercase lowercase letters
     * 
     * @param string $value
     * @throws sfValidatorError
     */
    private function haveLowercase($value)
    {
        preg_match_all('/[a-z|ñ]/', $value, $matches);

        if (count($matches[0]) >= $this->getOption('min_lowercase')) {

            return true;
        }

        throw new sfValidatorError($this, 'not_lowercase', array('min_lowercase' => $this->getOption('min_lowercase')));
    }

    /**
     * Check if $value have min_uppercase uppercase letters
     *
     * @param string $value
     * @throws sfValidatorError
     */
    private function haveUppercase($value)
    {
        preg_match_all('/[A-Z|Ñ]/', $value, $matches);

        if (count($matches[0]) >= $this->getOption('min_uppercase')) {

            return true;
        }

        throw new sfValidatorError($this, 'not_uppercase', array('min_uppercase' => $this->getOption('min_uppercase')));
    }

    /**
     * Check if $value have min_special_character special characters
     *
     * @param string $value
     * @throws sfValidatorError
     */
    private function haveSpecialCharacter($value)
    {
        preg_match_all('/[$|?|+|\-|!|%]/', $value, $matches);

        if (count($matches[0]) >= $this->getOption('min_special_character')) {

            return true;
        }

        throw new sfValidatorError(
            $this,
            'not_special_character',
            array('min_special_character' => $this->getOption('min_special_character')));
    }

    /**
     * Check if $value have min_number numbers
     *
     * @param string $value
     * @throws sfValidatorError
     */
    private function haveNumber($value)
    {
        preg_match_all('/[0-9]/', $value, $matches);

        if (count($matches[0]) >= $this->getOption('min_number')) {

            return true;
        }

        throw new sfValidatorError($this, 'not_number', array('min_number' => $this->getOption('min_number')));
    }
}
